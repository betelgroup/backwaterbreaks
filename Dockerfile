FROM maven:3.5.2-jdk-8-alpine AS MAVEN_TOOL_CHAIN
COPY pom.xml /build/
COPY backwaterbreaks-server/pom.xml /build/backwaterbreaks-server/
COPY backwaterbreaks-server/src /build/backwaterbreaks-server/src
COPY backwaterbreaks-test/pom.xml /build/backwaterbreaks-test/
#COPY backwaterbreaks-test/src /build/backwaterbreaks-test/src
WORKDIR /build/
RUN mvn package

FROM openjdk:8-jre-alpine
WORKDIR /app
COPY --from=MAVEN_TOOL_CHAIN /build/backwaterbreaks-server/target/backwaterbreaks-server-1.0-SNAPSHOT.jar /app/
ENTRYPOINT ["java", "-jar", "backwaterbreaks-server-1.0-SNAPSHOT.jar"]